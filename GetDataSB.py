#!/usr/bin/python
import sys,array,os
from ROOT import TFile,TTree,TChain,TBranch,TH1,TH1F,TH2F,TList,TH3,TH3F,TLine,TF1
from ROOT import TLorentzVector,TGraphAsymmErrors,TMath,TGraph,TMultiGraph
from ROOT import THStack,TCanvas,TLegend,TColor,TPaveText,TPad,TLatex
from ROOT import gStyle,gDirectory
from ROOT import Double
from math import sqrt,fabs,sin
from array import array

#filename = "Output/hadronic_datarw/splitted_list0000/ttHAna_histo.root"
filename = "Output/leptonic_datarw/splitted_list0000/ttHAna_histo.root"
fin = TFile(filename)

#for index in range(9):
for index in range(6):

   #hname = "NOM_cate{0}_Selected_m_yy".format(str(index+1))
   hname = "NOM_cate{0}_Selected_m_yy".format(str(index+10))
   #print hname
   h = fin.Get("NOM/{0}".format(hname))
   evts = h.Integral(1,150) + h.Integral(251,550)
   print int(evts)
   #print int(h.Integral())
