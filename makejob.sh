#!/bin/bash

# Before make run folders, get the absolute path for src/ and condor/
PATH_SRC=`readlink -f Code/src`
PATH_CONDOR=`readlink -f Code/condor`

# If no argument given, will use default runlist "runlist"; 
# If arguments given, will use them all.
if [ $# -ge 1 ];
  then runlists="$*"
else runlists="runlist"
fi

#############################
# Create Local folders     ##
# and Copy Scripts into it ##
#############################

# For each run list, prepare run folder and submit jobs
for this_runlist in ${runlists}; do

    # Read and get the path of runlist
    if [ -f "${this_runlist}" ]; then 
        runlist=${this_runlist}
        echo 
        echo "INFO => RunList: ${runlist}"
    elif [ -f "RunList/${this_runlist}" ]; then
        runlist="RunList/${this_runlist}"
        echo
        echo "INFO => RunList: ${runlist}"        
    else
        echo 
        echo "ERROR => Can not find RunList: ${this_runlist}, now skip it"
        continue
    fi    
    
    # For each dataset in runlist, prepare run folder and submit jobs 
    # Be cautious, the paths for dataset list should be absolute
    for dataset_list in `cat ${runlist}`; do
    
        echo 
        echo "INFO => Make job for Dataset: $dataset_list"
        localdir=`basename $dataset_list`

        # Create and Setup run folder, and Make jobs   
        if [ ! -d Output ]; then
           mkdir Output
        fi
 
        cd Output/          
        if [ ! -d "$localdir" ]; then
        
            mkdir $localdir
            # Make Directory and Copy Files
            echo "INFO => Create Run folder"      
            cd $localdir

            echo "INFO => Copy file list"
            cp $dataset_list  $localdir.input_list.txt

            echo "INFO => Copy condor scripts"
            cp $PATH_CONDOR/* .
            
            # Make Jobs
            echo "INFO => Make and run condor jobs"
            #./makejob-condor ${PATH_SRC} ${PATH_CONDOR}
            ./makejob-condor ${PATH_SRC} `pwd`
            # Done
            cd ..
        else
            echo "ERROR => $localdir exist !!!" 
            cd ..
            continue
        fi        
        
        # Go back to upper folder, prepare for next runlist
        cd ..
        
    done
    
    # Save run information into logfile
    if [ ! -d "Log" ]; then
        mkdir "Log"
    fi

    if [ ! -f "Log/log" ]; then
        echo "Auto Log file" > Log/log
        echo "Created by $USER on `date`" >> Log/log
    fi
    # backup the log first
    cp Log/log Log/log~

    # write this run info into the log file
    date=`date`

    echo >> Log/log
    echo "Date:     $date" >> Log/log
    echo "User:     $USER" >> Log/log
    echo "DataSet:  `cat ${runlist}`" >> Log/log

done

exit 0
