#!/usr/bin/python
import sys,array,os
from ROOT import TFile,TTree,TChain,TBranch,TH1,TH1F,TH2F,TList,TH3,TH3F,TLine,TF1
from ROOT import TLorentzVector,TGraphAsymmErrors,TMath,TGraph,TMultiGraph
from ROOT import THStack,TCanvas,TLegend,TColor,TPaveText,TPad,TLatex
from ROOT import gStyle,gDirectory
from ROOT import Double
from math import sqrt,fabs,sin
from array import array

process = ['ttH_PP8', 'ttH_PH7', 'ttH_a0_alt', 'ttH_a0', 'ttH_a15', 'ttH_a30', 'ttH_a45', 'ttH_a60', 'ttH_a75', 'ttH_a90', 'tWH_a0', 'tWH_a15', 'tWH_a30', 'tWH_a45', 'tWH_a60', 'tWH_a75', 'tWH_a90', 'tHjb_alt', 'tHjb_a0', 'tHjb_a15', 'tHjb_a30', 'tHjb_a45', 'tHjb_a60', 'tHjb_a75', 'tHjb_a90', 'ggH_PP8', 'ggH_a0', 'ggH_a45', 'ggH_a90', 'VBF', 'WH', 'ZH', 'ggZH', 'bbH', 'ttyy']

#for index in range(9):
for index in range(6):

   hname = "NOM_cate{0}_Selected_m_yy".format(str(index+10))
   print hname
   for proc in range(len(process)):
       #filename = "Output/hadronic_{0}rw/splitted_list0000/ttHAna_histo.root".format(process[proc])
       filename = "Output/leptonic_{0}rw/splitted_list0000/ttHAna_histo.root".format(process[proc])
       #print filename
       #os.system("readlink -f {0}".format(filename))
       fin = TFile(filename)
       h = fin.Get("NOM/{0}".format(hname))
       print round(h.Integral(), 4)
