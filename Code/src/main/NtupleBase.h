//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Nov  4 16:36:29 2019 by ROOT version 6.14/04
// from TTree output/output
// found on file: /eos/home-j/jdickins/ttHyy/cp-ntuples/102519/leptonic_10.8/ttHrw.root
//////////////////////////////////////////////////////////

#ifndef NtupleBase_h
#define NtupleBase_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>

// Headers needed by this particular selector


class NtupleBase : public TSelector {
public :
   TTreeReader     fReader;  //!the tree reader
   TTree          *fChain = 0;   //!pointer to the analyzed TTree or TChain

   // Readers to access the data (delete the ones you do not need).
   TTreeReaderValue<Double_t> pt_H = {fReader, "pt_H"};
   TTreeReaderValue<Double_t> eta_H = {fReader, "eta_H"};
   TTreeReaderValue<Double_t> t1_score = {fReader, "t1_score"};
   TTreeReaderValue<Double_t> t1_pt = {fReader, "t1_pt"};
   TTreeReaderValue<Double_t> t1_eta = {fReader, "t1_eta"};
   TTreeReaderValue<Double_t> t1_phi0 = {fReader, "t1_phi0"};
   TTreeReaderValue<Double_t> t2_score = {fReader, "t2_score"};
   TTreeReaderValue<Double_t> hy_pt = {fReader, "hy_pt"};
   TTreeReaderValue<Double_t> hy_eta = {fReader, "hy_eta"};
   TTreeReaderValue<Double_t> hy_phi0 = {fReader, "hy_phi0"};
   TTreeReaderValue<Double_t> delta_eta_t1hy = {fReader, "delta_eta_t1hy"};
   TTreeReaderValue<Double_t> delta_phi_t1hy = {fReader, "delta_phi_t1hy"};
   TTreeReaderValue<Double_t> m_t1hy = {fReader, "m_t1hy"};
   TTreeReaderValue<Double_t> m_HT = {fReader, "m_HT"};
   TTreeReaderValue<Int_t> m_njet = {fReader, "m_njet"};
   TTreeReaderValue<Int_t> m_nbjet_fixed80 = {fReader, "m_nbjet_fixed80"};
   TTreeReaderValue<Double_t> m_met_sig = {fReader, "m_met_sig"};
   TTreeReaderValue<Double_t> m_t1H = {fReader, "m_t1H"};
   TTreeReaderValue<Double_t> delta_Rmin_yj = {fReader, "delta_Rmin_yj"};
   TTreeReaderValue<Double_t> delta_Rmin_yj2 = {fReader, "delta_Rmin_yj2"};
   TTreeReaderValue<Int_t> eventNumber = {fReader, "eventNumber"};
   TTreeReaderValue<Double_t> weight_for_analysis = {fReader, "weight_for_analysis"};
   TTreeReaderValue<Int_t> fold = {fReader, "fold"};
   TTreeReaderValue<Int_t> m_mcChannelNumber = {fReader, "m_mcChannelNumber"};
   TTreeReaderValue<Double_t> m_mgg = {fReader, "m_mgg"};
   TTreeReaderValue<Bool_t> isPassed = {fReader, "isPassed"};
   TTreeReaderValue<Bool_t> flag_passedIso = {fReader, "flag_passedIso"};
   TTreeReaderValue<Bool_t> flag_passedPID = {fReader, "flag_passedPID"};
   TTreeReaderValue<Double_t> ph_pt1 = {fReader, "ph_pt1"};
   TTreeReaderValue<Double_t> ph_pt2 = {fReader, "ph_pt2"};
   TTreeReaderValue<Int_t> m_nlep = {fReader, "m_nlep"};
   TTreeReaderValue<Double_t> m_score_wisc = {fReader, "m_score_wisc"};
   TTreeReaderValue<Double_t> BDTG_cp_10_8_0 = {fReader, "BDTG_cp_10.8.0"};
   TTreeReaderValue<Int_t> BKG_category = {fReader, "BKG_category"};
   TTreeReaderValue<Int_t> CP_category = {fReader, "CP_category"};


   NtupleBase(TTree * /*tree*/ =0) { }
   virtual ~NtupleBase() { }
   virtual Int_t   Version() const { return 2; }
   virtual void    Begin(TTree *tree);
   virtual void    SlaveBegin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry);
   virtual Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
   virtual void    SetOption(const char *option) { fOption = option; }
   virtual void    SetObject(TObject *obj) { fObject = obj; }
   virtual void    SetInputList(TList *input) { fInput = input; }
   virtual TList  *GetOutputList() const { return fOutput; }
   virtual void    SlaveTerminate();
   virtual void    Terminate();

   ClassDef(NtupleBase,0);

};

#endif

#ifdef NtupleBase_cxx
void NtupleBase::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   fReader.SetTree(tree);
}

Bool_t NtupleBase::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}


#endif // #ifdef NtupleBase_cxx
