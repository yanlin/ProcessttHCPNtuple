//<AnalysisVar.C>
//<Aim to provide variables and objects for analysis>

#include "AnalysisVar.h"

//<AnalysisVar::ClearFlags>
void AnalysisVar::ClearFlags(MapType_Int& map) {

    MapType_Int::iterator it;
    for(it=map.begin(); it!=map.end(); it++) {
        string varname = (*it).first;
        map[varname] = 0;
    }        
}

void AnalysisVar::ClearFlags(MapType2_Int& map) {

    MapType2_Int::iterator it;
    for(it=map.begin(); it!=map.end(); it++) {
        MapType_Int::iterator it2;
        string chn=(*it).first;
        for(it2=(*it).second.begin(); it2!=(*it).second.end(); it2++) {
            string cut=(*it2).first;
            map[chn][cut] = 0;
        }
    }    
}

//<AnalysisVar::ClearVariables>
void AnalysisVar::ClearVariables(MapType_Double& map) {
    
    MapType_Double::iterator it;
    for(it=map.begin(); it!=map.end(); it++)
        map[(*it).first]=0.;
}

//<AnalysisVar::ClearVariables>
void AnalysisVar::ClearVariables(MapType_VDoubleStar& map) {

    MapType_VDoubleStar::iterator it;
    for(it=map.begin(); it!=map.end(); it++)
        map[(*it).first]->clear();

}

//<AnalysisVar::ClearWeight>
void AnalysisVar::ClearWeight(MapType_VDouble& map) {
    MapType_VDouble::iterator it;
    for(it=map.begin(); it!=map.end(); it++)
        for(int i=0; i<(int)map[(*it).first].size();i++)
            map[(*it).first].at(i)=1.0;
}

//<AnalysisVar::CreateCountingMap>
void AnalysisVar::CreateCountingMap() {
    
    //<Event Counting>
    for(int i=0; i<(int)CHN.size(); i++) {
        string chn=CHN[i];
		// for reco. event selection counting
        for(int j=0; j<(int)STEP_cut.size(); j++) {
            string cut=STEP_cut[j];
            for(int k=0; k<(int)SETNAME.size();k++) {
                COUNT ini={0.,0.};
                CNT_cut[SETNAME[k]][chn][cut]=ini;
            }
            FLAG_cut[chn][cut]=FLAG_cut_temp[chn][cut]=0;
        }
		// for truth event selection counting
		for (int j = 0; j<(int)STEP_cut_truth.size(); j++) {
			string cut = STEP_cut_truth[j];
			for (int k = 0; k<(int)SETNAME_TRUTH.size(); k++) {
				COUNT ini = { 0., 0. };
				CNT_cut_truth[SETNAME_TRUTH[k]][chn][cut] = ini;
			}
			FLAG_cut_truth[chn][cut] = FLAG_cut_temp_truth[chn][cut] = 0;
		}
    }
    //<Object Counting>
    MapType_VString::iterator it;

	// for reco. obj selection counting
    for(it=STEP_obj.begin(); it!=STEP_obj.end(); it++) {
        string obj=(*it).first;
        for(int i=0; i<(int)(*it).second.size(); i++) {
            string cut=STEP_obj[obj][i];
            for(int k=0; k<(int)SETNAME.size();k++) {
               COUNT ini={0.,0.};
               CNT_obj[SETNAME[k]][obj][cut]=ini;
            }
            FLAG_obj[obj][cut]=FLAG_obj_temp[obj][cut]=0;
        }
    }

	// for truth. obj selection counting
	for (it = STEP_obj_truth.begin(); it != STEP_obj_truth.end(); it++) {
		string obj = (*it).first;
		for (int i = 0; i<(int)(*it).second.size(); i++) {
			string cut = STEP_obj_truth[obj][i];
			for (int k = 0; k<(int)SETNAME_TRUTH.size(); k++) {
				COUNT ini = { 0., 0. };
				CNT_obj_truth[SETNAME_TRUTH[k]][obj][cut] = ini;
			}
			FLAG_obj_truth[obj][cut] = FLAG_obj_temp_truth[obj][cut] = 0;
		}
	}
    
    //<Weight Map, currently set 8 bits, if not enough can add>

	// for event weight map in reco. selection 
    // specifically for histograms filled multiple times in one event
    // i.e. the histogram variable is a vector
    // first-index gives the weight for first fill
    //...
    for(int j=0; j<(int)STEP_cut.size(); j++) {
        string cut=STEP_cut[j];
        vector<double> weight;
        weight.push_back(1.0);
        weight.push_back(1.0);
        weight.push_back(1.0);
        weight.push_back(1.0);
        weight.push_back(1.0);
        weight.push_back(1.0);
        weight.push_back(1.0);
        weight.push_back(1.0);
        Evt_Weight_Vec[cut]=weight;
    }    
    
    
	// for event weight map in reco. selection
    for(int j=0; j<(int)STEP_cut.size(); j++) {
        string cut=STEP_cut[j];
        vector<double> weight;
        weight.push_back(1.0);
        weight.push_back(1.0);
        weight.push_back(1.0);
        weight.push_back(1.0);
        weight.push_back(1.0);
        weight.push_back(1.0);
        weight.push_back(1.0);
        weight.push_back(1.0);
        Evt_Weight[cut]=weight;
    }

	// for event weight map in truth selection
	for (int j = 0; j<(int)STEP_cut_truth.size(); j++) {
		string cut = STEP_cut_truth[j];
		vector<double> weight;
		weight.push_back(1.0);
		weight.push_back(1.0);
		weight.push_back(1.0);
		weight.push_back(1.0);
		weight.push_back(1.0);
		weight.push_back(1.0);
		weight.push_back(1.0);
		weight.push_back(1.0);
		Evt_Weight_truth[cut] = weight;
	}
    
}

//<AnalysisVar::SetFlag>
void AnalysisVar::SetFlag(MapType2_Int& map, string cut, string chn, int value) {

    if(chn=="All" || chn=="ALL" || chn=="all") {
        for(int i=0; i<(int)CHN.size(); i++) {
            string chn1=CHN[i];
            map[chn1][cut]=value;
        }
    }
    else {
       map[chn][cut]=value;
    } 
}

//<AnalysisVar::DoCounting>
void AnalysisVar::DoCounting(vector<string> STEP, MapType2_Int FLAG_temp, MapType2_Int& FLAG, MapType2_Counting& CNT, MapType_VDouble& Weight, string chn, int applyweight) {

    int pass=1;
    for(int i=0; i<(int)STEP.size(); i++) {
        string cut = STEP[i];
        double wt_all=1.0;
        if(applyweight==1) {
            for(int j=0; j<(int)Weight[cut].size();j++)
                wt_all *= Weight[cut][j];
        }
        if(pass==0) continue;
        pass = pass && FLAG_temp[chn][cut];
        if(pass==1) {
            FLAG[chn][cut]=1;
            CNT[chn][cut].num += wt_all * 1.0; 
            CNT[chn][cut].err = sqrt(pow(CNT[chn][cut].err,2)+pow(wt_all,2));
        }
    }
}

void AnalysisVar::DoCountingCut(string setname) {

    //if no setname in SETNAME vector, raise error
    if(find(SETNAME.begin(),SETNAME.end(),setname)==SETNAME.end()) {
        cout<<"Error => Can not find setting "<<setname<<endl;
        exit(-1);
    }

    for(int i=0; i<(int)CHN.size(); i++)
        if(CHN[i]!="incl")
            DoCounting(STEP_cut,FLAG_cut_temp,FLAG_cut,CNT_cut[setname],Evt_Weight,CHN[i],1);

    for(int i=0; i<(int)CHN.size(); i++)
        for(int j=0; j<(int)STEP_cut.size(); j++) {
            string chn2=CHN[i];
            string cut=STEP_cut[j];
            FLAG_cut_temp["incl"][cut]=FLAG_cut_temp["incl"][cut] || FLAG_cut[chn2][cut];
        }
    DoCounting(STEP_cut,FLAG_cut_temp,FLAG_cut,CNT_cut[setname],Evt_Weight,"incl",1);         
        
}

void AnalysisVar::DoCountingCutTruth(string setname) {

	//if no setname in SETNAME vector, raise error
	if (find(SETNAME_TRUTH.begin(), SETNAME_TRUTH.end(), setname) == SETNAME_TRUTH.end()) {
		cout << "Error => Can not find setting " << setname << endl;
		exit(-1);
	}

	for (int i = 0; i<(int)CHN.size(); i++)
		if (CHN[i] != "incl")
			DoCounting(STEP_cut_truth, FLAG_cut_temp_truth, FLAG_cut_truth, CNT_cut_truth[setname], Evt_Weight_truth, CHN[i], 1);

	for (int i = 0; i<(int)CHN.size(); i++)
		for (int j = 0; j<(int)STEP_cut_truth.size(); j++) {
		string chn2 = CHN[i];
		string cut = STEP_cut_truth[j];
		FLAG_cut_temp_truth["incl"][cut] = FLAG_cut_temp_truth["incl"][cut] || FLAG_cut_truth[chn2][cut];
		}
	DoCounting(STEP_cut_truth, FLAG_cut_temp_truth, FLAG_cut_truth, CNT_cut_truth[setname], Evt_Weight_truth, "incl", 1);


}

void AnalysisVar::DoCountingObj(string obj, string setname) {
    
    //if no setname in SETNAME vector, raise error
    if(find(SETNAME.begin(),SETNAME.end(),setname)==SETNAME.end()) {
        cout<<"Error => Can not find setting "<<setname<<endl;
        exit(-1);
    }

    if(obj=="All" || obj=="ALL" || obj=="all") {
        MapType_VString::iterator it;
        for(it=STEP_obj.begin(); it!=STEP_obj.end(); it++)
            DoCounting((*it).second, FLAG_obj_temp, FLAG_obj, CNT_obj[setname], Evt_Weight, (*it).first,0);
    }
    else {
        DoCounting(STEP_obj[obj], FLAG_obj_temp, FLAG_obj, CNT_obj[setname], Evt_Weight, obj, 0);
    }
}

void AnalysisVar::DoCountingObjTruth(string obj, string setname) {

	//if no setname in SETNAME vector, raise error
	if (find(SETNAME_TRUTH.begin(), SETNAME_TRUTH.end(), setname) == SETNAME_TRUTH.end()) {
		cout << "Error => Can not find setting " << setname << endl;
		exit(-1);
	}

	if (obj == "All" || obj == "ALL" || obj == "all") {
		MapType_VString::iterator it;
		for (it = STEP_obj_truth.begin(); it != STEP_obj_truth.end(); it++)
			DoCounting((*it).second, FLAG_obj_temp_truth, FLAG_obj_truth, CNT_obj_truth[setname], Evt_Weight_truth, (*it).first, 0);
	}
	else {
		DoCounting(STEP_obj_truth[obj], FLAG_obj_temp_truth, FLAG_obj_truth, CNT_obj_truth[setname], Evt_Weight_truth, obj, 0);
	}
}

//<AnalysisVar::CreateTreeVarHistoMap>
void AnalysisVar::CreateTreeVarHistoMap() {

    //Check that histo file must exist
    if(!HistoFile) {
        cout<<"ERROR => Histogram file not exist !!!"<<endl;
        cout<<"ERROR => Now quitting"<<endl;
    }
    HistoFile->cd();
    
    //Create subfolders in the root file according to setting names
    for(int i=0; i<(int)SETNAME.size();i++) {
        HistoFile->mkdir(SETNAME[i].c_str());
    }
	for (int i = 0; i < (int)SETNAME_TRUTH.size(); i++) {
		HistoFile->mkdir(SETNAME_TRUTH[i].c_str());
	}

    // TreeVar & 1-Dimension histogram
    MapType2_Double::iterator it;
    for(it=VarName.begin(); it!=VarName.end(); it++) {
        //<Prepare Variables>
        string varname=(*it).first;
        //<Create Tree Variable First>
		//Read various setup information from maps
        int nbin=0, option=0, cutstep=0, vec=0;
        float xlow=0., xhigh=0.;
		bool istruth = false;
		int cutsteptruth = 0;
        nbin = int((*it).second["NBins"]);
        xlow = double((*it).second["Xmin"]);
        xhigh = double((*it).second["Xmax"]);
        option = int((*it).second["Option"]);
        cutstep = int((*it).second["CutStep"]);
        vec = int((*it).second["Vector"]);
		istruth = bool((*it).second["IsTruth"]);
		cutsteptruth = int((*it).second["CutStepTruth"]);
        //<Initialize variable>
        if(vec==1) {
            vector<double>* tmp = new vector<double>();
            VVar[varname] = tmp;
			vector<double>* tmp2 = new vector<double>();
			VVarStore[varname] = tmp2;
        }
		else {
			Var[varname] = 0.;
			VarStore[varname] = 0.;
		}
        if(option==InTreeOnly) continue;
        //<Create Histograms>
        for(int i=0; i<(int)CHN.size(); i++) {
            string chn=CHN[i];

			// For normal reconstruction level selection sets
            for(int j=0; j<(int)STEP_cut.size(); j++) {
				//if donot want to plot at this step, skip
                if( j<cutstep || (j!=-1*cutstep && cutstep<0) ) continue;  
				//loop the selected cut steps and create histograms accordingly
                string cut=STEP_cut[j];
                for(int k=0; k<(int)SETNAME.size();k++) {
                    if(SETTING[SETNAME[k]]["dohistogram"]==0) continue;
                    HistoFile->cd(SETNAME[k].c_str());
                    string histo_name= SETNAME[k] + "_" + chn + "_" + cut + "_" + varname;
                    //TH1F *histo_pointer = new TH1F(histo_name.c_str(),histo_name.c_str(),nbin,xlow,xhigh);
                    TH1F *histo_pointer = new TH1F(histo_name.c_str(),histo_name.c_str(),nbin,&VarBinning[varname][0]);
                    histo_pointer->Sumw2();
                    histo[SETNAME[k]][chn][cut][varname]=histo_pointer;
                 }//for::k
            }//for:j

			// For truth level selection sets
			for (int j = 0; j<(int)STEP_cut_truth.size(); j++) {
				//if donot want to plot at this step, skip
				if (j<cutsteptruth || (j != -1 * cutsteptruth && cutsteptruth<0)) continue;
				//if not truth variable, skip
				if (!istruth) continue;
				//loop the selected cut steps and create histograms accordingly
				string cut = STEP_cut_truth[j];
				for (int k = 0; k<(int)SETNAME_TRUTH.size(); k++) {
					if (SETTING_TRUTH[SETNAME_TRUTH[k]]["dohistogram"] == 0) continue;
					HistoFile->cd(SETNAME_TRUTH[k].c_str());
					string histo_name = SETNAME_TRUTH[k] + "_" + chn + "_" + cut + "_" + varname;
					//TH1F *histo_pointer = new TH1F(histo_name.c_str(), histo_name.c_str(), nbin, xlow, xhigh);
					TH1F *histo_pointer = new TH1F(histo_name.c_str(), histo_name.c_str(), nbin, &VarBinning[varname][0]);
					histo_pointer->Sumw2();
					histo[SETNAME_TRUTH[k]][chn][cut][varname] = histo_pointer;
				}//for::k
			}//for:j

        }//for:i
    }// for:it
    
    // Create 2-Dimension histogram
	// Temporarily only available for reco level vars
    for(int i=0; i<(int)CHN.size(); i++) {
        string chn=CHN[i];
        for(int j=0; j<(int)STEP_cut.size(); j++) {
            string cut=STEP_cut[j];    
            MapType2_VDouble::iterator it2;    
            for(it2=helper_2D.begin(); it2!=helper_2D.end(); it2++) {
                int should_keep=0;
				// get the histogram name, chn_cut_var1_var2, base_name=var1_var2
                string base_name=(*it2).first;
                string histo2D_name = chn + "_" + cut + "_" + base_name;       
                vector<double> var1_range, var2_range;
                MapType_VDouble::iterator it3;
				// check the two variables
                for(it3=(*it2).second.begin(); it3!=(*it2).second.end(); it3++) {
                    string name = (*it3).first;
                    vector<double> setting = (*it3).second;
					//if donot want to plot at this step, skip
                    if((j>=setting[setting.size()-1] && setting[setting.size()-1]>=0) || (j==-1*setting[setting.size()-1] && setting[setting.size()-1]<0)) should_keep=1;
					//define binning for var1
                    if(setting[0]==1) {
                        //var1_range.push_back(setting[1]);
                        //var1_range.push_back(setting[2]);
                        //var1_range.push_back(setting[3]);
                        for(int index=2; index<3+setting[1]; index++)
                            var1_range.push_back(setting[index]);
                    }
					//define binning for var2
                    if(setting[0]==2) {
                        //var2_range.push_back(setting[1]);
                        //var2_range.push_back(setting[2]);
                        //var2_range.push_back(setting[3]);
                        for(int index=2; index<3+setting[1]; index++)
                            var2_range.push_back(setting[index]);                        
                    }
                }
                if(!should_keep) continue;

				//create the histograms
                for(int k=0; k<(int)SETNAME.size();k++) {
                    if(SETTING[SETNAME[k]]["dohistogram"]==0) continue;
                    HistoFile->cd(SETNAME[k].c_str());
                    string histo2D_name_f;
                    histo2D_name_f = SETNAME[k] + "_" + histo2D_name;
                    //TH2F *histo2D_pointer = new TH2F(histo2D_name_f.c_str(),histo2D_name_f.c_str(),\
                                                    (int)var1_range[0],var1_range[1],var1_range[2],\
                                                    (int)var2_range[0],var2_range[1],var2_range[2]);
                    TH2F *histo2D_pointer = new TH2F(histo2D_name_f.c_str(),histo2D_name_f.c_str(),\
                                                    (int)var1_range.size()-1, &var1_range[0],
                                                    (int)var2_range.size()-1, &var2_range[0]);                                                
                    histo2D_pointer->Sumw2();
                    histo_2D[SETNAME[k]][chn][cut][base_name]=histo2D_pointer;
                }
            }
        }
    }
    
    
    // Create 3-Dimension histogram
	// Temporarily only available for reco level vars
    for(int i=0; i<(int)CHN.size(); i++) {
        string chn=CHN[i];
        for(int j=0; j<(int)STEP_cut.size(); j++) {
            string cut=STEP_cut[j];    
            MapType2_VDouble::iterator it2;    
            for(it2=helper_3D.begin(); it2!=helper_3D.end(); it2++) {
                int should_keep=0;
				// get the histogram name, chn_cut_var1_var2, base_name=var1_var2
                string base_name=(*it2).first;
                string histo3D_name = chn + "_" + cut + "_" + base_name;       
                vector<double> var1_range, var2_range, var3_range;
                MapType_VDouble::iterator it3;
				// check the two variables
                for(it3=(*it2).second.begin(); it3!=(*it2).second.end(); it3++) {
                    string name = (*it3).first;
                    vector<double> setting = (*it3).second;
					//if donot want to plot at this step, skip
                    if((j>=setting[setting.size()-1] && setting[setting.size()-1]>=0) || (j==-1*setting[setting.size()-1] && setting[setting.size()-1]<0)) should_keep=1;
					//define binning for var1
                    if(setting[0]==1) {
                        //var1_range.push_back(setting[1]);
                        //var1_range.push_back(setting[2]);
                        //var1_range.push_back(setting[3]);
                        for(int index=2; index<3+setting[1]; index++)
                            var1_range.push_back(setting[index]);
                    }
					//define binning for var2
                    if(setting[0]==2) {
                        //var2_range.push_back(setting[1]);
                        //var2_range.push_back(setting[2]);
                        //var2_range.push_back(setting[3]);
                        for(int index=2; index<3+setting[1]; index++)
                            var2_range.push_back(setting[index]);                        
                    }
					//define binning for var2
                    if(setting[0]==3) {
                        //var3_range.push_back(setting[1]);
                        //var3_range.push_back(setting[2]);
                        //var3_range.push_back(setting[3]);
                        for(int index=2; index<3+setting[1]; index++)
                            var3_range.push_back(setting[index]);                        
                    }                    
                }
                if(!should_keep) continue;

				//create the histograms
                for(int k=0; k<(int)SETNAME.size();k++) {
                    if(SETTING[SETNAME[k]]["dohistogram"]==0) continue;
                    HistoFile->cd(SETNAME[k].c_str());
                    string histo3D_name_f;
                    histo3D_name_f = SETNAME[k] + "_" + histo3D_name;
                    //TH2F *histo2D_pointer = new TH2F(histo2D_name_f.c_str(),histo2D_name_f.c_str(),\
                                                    (int)var1_range[0],var1_range[1],var1_range[2],\
                                                    (int)var2_range[0],var2_range[1],var2_range[2]);
                    TH3F *histo3D_pointer = new TH3F(histo3D_name_f.c_str(),histo3D_name_f.c_str(),\
                                                    (int)var1_range.size()-1, &var1_range[0],
                                                    (int)var2_range.size()-1, &var2_range[0],
                                                    (int)var3_range.size()-1, &var3_range[0]);                                                
                    histo3D_pointer->Sumw2();
                    histo_3D[SETNAME[k]][chn][cut][base_name]=histo3D_pointer;
                }
            }
        }
    }    
    
}

//<AnalysisVar::StoreTruthVariables>
void AnalysisVar::StoreTruthVariables(string setname) {
	
	//copy the Var and VVar vectors into VarStore and VVarStore for given truth setnames
	MapType2_Double::iterator it;
	for (it = VarName.begin(); it != VarName.end(); it++) {
		string varname = (*it).first;
		//only handle truth variables, if not, skip
		if (!(bool)VarName[varname]["IsTruth"]) continue;
		//copy
		if ((bool)VarName[varname]["Vector"]) {
			VVarStore[varname]->clear();
			for (int k = 0; k < VVar[varname]->size(); k++)
				VVarStore[varname]->push_back(VVar[varname]->at(k));
		}
		else {
			VarStore[varname] = Var[varname];
		}

	} //for VarName

}

//<AnalysisVar::AddVarIntoTree>
void AnalysisVar::AddVarIntoTree(TTree *tree) {

    //<loop the "treeVar" map to automatically add branches to tree>
    //<To make life easier, all variables are set to be "double">
    MapType2_Double::iterator it;
    for(it=VarName.begin(); it!=VarName.end(); it++) {
        string varname = (*it).first;
        if(VarName[varname]["Vector"])
            tree->Branch(varname.c_str(),"vector<double>",&VVar[varname]);
        else
            tree->Branch(varname.c_str(),&Var[varname]);
    }
    
    //<Also add event selection flags into trees for further look>
	//Part I: for reco. 
    MapType2_Int::iterator it2;
    for(it2=FLAG_cut.begin(); it2!=FLAG_cut.end(); it2++) {
        string chn=(*it2).first;
        MapType_Int::iterator it3;
        for(it3=(*it2).second.begin(); it3!=(*it2).second.end(); it3++) {
            string cut=(*it3).first;
            string varname="FLAG_"+chn+"_"+cut;
            string varname2="FLAG_"+chn+"_"+cut+"/I";
            tree->Branch(varname.c_str(),&FLAG_cut[chn][cut],varname2.c_str());
        }
    }
	//Part II: for truth
	for (it2 = FLAG_cut_truth.begin(); it2 != FLAG_cut_truth.end(); it2++) {
		string chn = (*it2).first;
		MapType_Int::iterator it3;
		for (it3 = (*it2).second.begin(); it3 != (*it2).second.end(); it3++) {
			string cut = (*it3).first;
			string varname = "FLAG_" + chn + "_" + cut;
			string varname2 = "FLAG_" + chn + "_" + cut + "/I";
			tree->Branch(varname.c_str(), &FLAG_cut_truth[chn][cut], varname2.c_str());
		}
	}

}

//<AnalysisVar::Fill1DHistograms>
void AnalysisVar::Fill1DHistograms(string setname, bool isReco, vector<string> step, MapType2_Int flag, MapType_VDouble weight) {

	//<fill 1D histogram>
	MapType2_Double::iterator it;
	for (it = VarName.begin(); it != VarName.end(); it++) {
		string varname = (*it).first;
		//check if in tree only
		if (VarName[varname]["Option"] == InTreeOnly) continue;

		// skip the reco variables if running truth selection
		if (!isReco && !(bool)VarName[varname]["IsTruth"]) continue;

		// get the required cutstep
		int cutstep = -1;
		if (isReco) cutstep = (int)VarName[varname]["CutStep"];
		else cutstep = (int)VarName[varname]["CutStepTruth"];

		for (int j = 0; j<(int)step.size(); j++) {
			string cut = step[j];
			//skip the cut steps if not specified
			if (j<cutstep || (j != -1 * cutstep && cutstep<0)) continue;

			// loop channels
			for (int i = 0; i<(int)CHN.size(); i++) {
				string chn = CHN[i];
				double wt_all = 1.0, wt_nopileup = 1.0, wt_nosf = 1.0, wt_nosfnopileup = 1.0, wt_histo = 1.0;
				wt_all = weight[cut][0] * weight[cut][1] * weight[cut][2] * weight[cut][3] * weight[cut][4]
					* weight[cut][5] * weight[cut][6] * weight[cut][7];
				wt_nopileup = weight[cut][0] * weight[cut][2] * weight[cut][3];
				wt_nosf = weight[cut][0] * weight[cut][1] * weight[cut][3];
				wt_nosfnopileup = weight[cut][0] * weight[cut][3];
                                //cout<<"chn: "<<chn<<" cut: "<<cut<<" wt_all: "<<wt_all<<" flag[chn][cut]: "<<flag[chn][cut]<<endl;
				//fill only if the event can pass given criteria
				if (flag[chn][cut]) {
					//get weights
					if (VarName[varname]["Option"] == InBothNoPileup) wt_histo = wt_nopileup;
					else if (VarName[varname]["Option"] == InBothNoSF) wt_histo = wt_nosf;
					else if (VarName[varname]["Option"] == InBothNoPileupNoSF) wt_histo = wt_nosfnopileup;
					else wt_histo = wt_all;
					//handle if variable is single or vector
					vector<double> tmp;
					if (VarName[varname]["Vector"]) {
						for (int k = 0; k < (int)VVar[varname]->size(); k++) {
							//if want to fill truth variable into reco running
							if (isReco && (bool)VarName[varname]["IsTruth"]) {
								tmp.push_back(VVarStore[varname]->at(k));
							}
							//normal occasion
							else {
								tmp.push_back(VVar[varname]->at(k));
							}
						}
					}
					else {
						//if want to fill truth variable into reco running
						if (isReco && (bool)VarName[varname]["IsTruth"]) {
							tmp.push_back(VarStore[varname]);
						}
						//normal occasion
						else {
							tmp.push_back(Var[varname]);
						}
					}
					//fill histogram
					for (int k = 0; k < (int)tmp.size(); k++) {
                                                //cout<<"wt_histo: "<<wt_histo<<endl;
                                                if(chn == "osee" || chn == "ttosee" || chn == "osem"){
                                                  //cout<<"cut: "<<cut<<" wt_histo: "<<wt_histo<<endl;
				                histo[setname][chn][cut][varname]->Fill(tmp[k], wt_histo);
                                                }
                                                else histo[setname][chn][cut][varname]->Fill(tmp[k], wt_histo);
                                                //cout<<"1D weight: "<<wt_histo<<endl;
					}
				}
			} // for i
		} // for j
	}

}

//<AnalysisVar::FillHistograms>
void AnalysisVar::FillHistograms(string setname) {

    //if no setname in SETNAME vector, raise error
	bool RecoSet = !(find(SETNAME.begin(), SETNAME.end(), setname) == SETNAME.end());
	bool TruthSet = !(find(SETNAME_TRUTH.begin(), SETNAME_TRUTH.end(), setname) == SETNAME_TRUTH.end());
    if(!RecoSet && !TruthSet) {
        cout<<"Error => Can not find setting "<<setname<<endl;
        exit(-1);
    }

	//if don't want to fill histogram, skip
	if (RecoSet && SETTING[setname]["dohistogram"] == 0) return;
	if (TruthSet && SETTING_TRUTH[setname]["dohistogram"] == 0) return;

	//Fill 1D histograms using "Fill1DHistograms" function
	if (RecoSet) Fill1DHistograms(setname, true, STEP_cut, FLAG_cut, Evt_Weight);
	if (TruthSet) Fill1DHistograms(setname, false, STEP_cut_truth, FLAG_cut_truth, Evt_Weight_truth);

	if (TruthSet) return; // for the moment, do not fill 2D histograms for truth variables

    //<fill 2D histogram>
	//2D histograms for reco vars only: temporarily
	//Consider the 2D histograms are onlt desired for reco level selections, can include both reco and truth variables.
    MapType2_VDouble::iterator it2;            
    for(it2=helper_2D.begin(); it2!=helper_2D.end(); it2++) {
        string base_name = (*it2).first;
        string var1,var2;
        MapType_VDouble::iterator it3;
        int cutstep=0;

        //get the x,y component varname
        for(it3=(*it2).second.begin(); it3!=(*it2).second.end(); it3++) {
            string name = (*it3).first;
            vector<double> setting = (*it3).second;        
            if(setting[0]==1) {var1=name;}
            if(setting[0]==2) {var2=name;}
            cutstep=(int)setting[setting.size()-1];
        }
        //if any of the two is vector, then the two must have the same length
        if(VarName[var1]["Vector"] || VarName[var2]["Vector"]) {
            if(VVar[var1]->size() != VVar[var2]->size()) {
                cout<<"Error => This 2D histogram can't be filled, since vector length doesnt match, "
                <<base_name<<endl;
                exit(-1);
            }
        }

        //fill 2d
        for(int i=0; i<(int)STEP_cut.size(); i++) {
            string cut = STEP_cut[i];
            if(i<cutstep || (i!=-1*cutstep && cutstep<0)) continue;
            for(int j=0; j<(int)CHN.size(); j++) {
                string chn = CHN[j];
                double wt_all=1.0;
                wt_all=Evt_Weight[cut][0]*Evt_Weight[cut][1]*Evt_Weight[cut][2]*Evt_Weight[cut][3]*Evt_Weight[cut][4]
					* Evt_Weight[cut][5] * Evt_Weight[cut][6] * Evt_Weight[cut][7];
                if(FLAG_cut[chn][cut]) {
					if (VarName[var1]["IsTruth"] && !VarName[var2]["IsTruth"]) {
						//Directly fill in if this is normal variable, Use stored variables if this is truth variables
						histo_2D[setname][chn][cut][base_name]->Fill(VarStore[var1], Var[var2], wt_all);
					}
					if (!VarName[var1]["IsTruth"] && VarName[var2]["IsTruth"]) {
						//Directly fill in if this is normal variable, Use stored variables if this is truth variables
						histo_2D[setname][chn][cut][base_name]->Fill(Var[var1], VarStore[var2], wt_all);
					}
					if (!VarName[var1]["IsTruth"] && !VarName[var2]["IsTruth"]) {
						//Directly fill in if all normal variables
                        if(VarName[var1]["Vector"] && VarName[var2]["Vector"]) {
                            for(int index = 0; index<(int)VVar[var1]->size(); index++) 
                                //special weights provided for filling multiple times
                                histo_2D[setname][chn][cut][base_name]->Fill(VVar[var1]->at(index), VVar[var2]->at(index), Evt_Weight_Vec[cut][index]);
                        }
                        else
                            histo_2D[setname][chn][cut][base_name]->Fill(Var[var1], Var[var2], wt_all);
                          //cout<<"2D weight: "<<wt_all<<endl;
					}
				}
            }
        }
    }
    
    
    //<fill 3D histogram>
	//3D histograms for reco vars only: temporarily
	//Consider the 3D histograms are onlt desired for reco level selections, can include both reco and truth variables.
    for(it2=helper_3D.begin(); it2!=helper_3D.end(); it2++) {
        string base_name = (*it2).first;
        string var1,var2,var3;
        MapType_VDouble::iterator it3;
        int cutstep=0;

        //get the x,y component varname
        for(it3=(*it2).second.begin(); it3!=(*it2).second.end(); it3++) {
            string name = (*it3).first;
            vector<double> setting = (*it3).second;        
            if(setting[0]==1) {var1=name;}
            if(setting[0]==2) {var2=name;}
            if(setting[0]==3) {var3=name;}
            cutstep=(int)setting[setting.size()-1];
        }
        //if any of the two is vector, then the two must have the same length
        if(VarName[var1]["Vector"] || VarName[var2]["Vector"] || VarName[var3]["Vector"]) {
            if(VVar[var1]->size() != VVar[var2]->size() || VVar[var1]->size() != VVar[var3]->size()) {
                cout<<"Error => This 3D histogram can't be filled, since vector length doesnt match, "
                <<base_name<<endl;
                exit(-1);
            }
        }

        //fill 3D
        for(int i=0; i<(int)STEP_cut.size(); i++) {
            string cut = STEP_cut[i];
            if(i<cutstep || (i!=-1*cutstep && cutstep<0)) continue;
            for(int j=0; j<(int)CHN.size(); j++) {
                string chn = CHN[j];
                double wt_all=1.0;
                wt_all=Evt_Weight[cut][0]*Evt_Weight[cut][1]*Evt_Weight[cut][2]*Evt_Weight[cut][3]*Evt_Weight[cut][4]
					* Evt_Weight[cut][5] * Evt_Weight[cut][6] * Evt_Weight[cut][7];
                if(FLAG_cut[chn][cut]) {
                    /*
					if (VarName[var1]["IsTruth"] && !VarName[var2]["IsTruth"]) {
						//Directly fill in if this is normal variable, Use stored variables if this is truth variables
						histo_3D[setname][chn][cut][base_name]->Fill(VarStore[var1], Var[var2], wt_all);
					}
					if (!VarName[var1]["IsTruth"] && VarName[var2]["IsTruth"]) {
						//Directly fill in if this is normal variable, Use stored variables if this is truth variables
						histo_3D[setname][chn][cut][base_name]->Fill(Var[var1], VarStore[var2], wt_all);
					}
                    */
					if (!VarName[var1]["IsTruth"] && !VarName[var2]["IsTruth"] && !VarName[var3]["IsTruth"]) {
						//Directly fill in if all normal variables
                        if(VarName[var1]["Vector"] && VarName[var2]["Vector"] && VarName[var3]["Vector"]) {
                            for(int index = 0; index<(int)VVar[var1]->size(); index++) 
                                //special weights provided for filling multiple times
                                histo_3D[setname][chn][cut][base_name]->Fill(VVar[var1]->at(index), VVar[var2]->at(index), VVar[var3]->at(index), Evt_Weight_Vec[cut][index]);
                        }
                        else
                            histo_3D[setname][chn][cut][base_name]->Fill(Var[var1], Var[var2], Var[var3], wt_all);
					}
				}
            }
        }
    }    
    
    
    
}

//<AnalysisVar::CreateRootFile>
TFile* AnalysisVar::CreateRootFile(string filename) {

    TFile *rootfile = new TFile(filename.c_str(), "recreate"); 
    if(!rootfile) {
        cout<<"ERROR => Can not open RootFile "<<filename<<endl;
        exit(-1);        
    }
    return rootfile;
    
}

//<AnalysisVar::CreateAllFiles>
void AnalysisVar::CreateAllFiles(string filename) {

    //string filename1 = filename + "_tree.root";
    //TreeFile = CreateRootFile(filename1);
    string filename2 = filename + "_histo.root";
    HistoFile = CreateRootFile(filename2);
    
}  
    
//<AnalysisVar::InitStrVec>
void AnalysisVar::InitStrVec(vector<string>& out, string in, string de) {
    int pos=0, pos_pre=0;
    while(true) {
        pos=in.find(de,pos_pre);
        if(pos==-1) {out.push_back(in.substr(pos_pre,in.size()-pos_pre)); break;}
        else out.push_back(in.substr(pos_pre,pos-pos_pre));
        pos_pre=pos+1;
    }
}

//<AnalysisVar::ToString>
string AnalysisVar::ToString(int number) {
    stringstream ss;
    ss << number;
    return ss.str();
}
string AnalysisVar::ToString(double number) {
    stringstream ss;
    ss << number;
    return ss.str();
}

//<AnalysisVar::InitSTEP>
void AnalysisVar::InitSTEP(vector<string>& STEP, string steps) {
    InitStrVec(STEP, steps, ",");
}
void AnalysisVar::InitSTEP(MapType_VString& STEP, string obj, string steps) {
    vector<string> str;
    InitStrVec(str, steps, ",");
    STEP[obj]=str;
}

//<AnalysisVar::InitVar>
void AnalysisVar::InitVar(string varlist, int nbin, double xmin, double xmax, int cutstep, int option, int vec, bool istruth, int cutstep_truth) {
    vector<string> variables;
    InitStrVec(variables, varlist, ",");
    for(int i=0; i<(int)variables.size(); i++) {
		// Note that "CutStep" specify at which cut stage the histogram will be filled
		// For truth variables with IsTruth==true, the "CutStepTruth" will specify where to fill in truth selection
		// Please note by default the truth variables will be automatically filled at reco runnings at the step indicated by "CutStep"
        VarName[variables[i]]["NBins"]=nbin;
        VarName[variables[i]]["Xmin"]=xmin;
        VarName[variables[i]]["Xmax"]=xmax;
        VarName[variables[i]]["Option"]=(double)option;        
        VarName[variables[i]]["CutStep"]=(double)cutstep;   
        VarName[variables[i]]["Vector"]=(double)vec;
		VarName[variables[i]]["IsTruth"] = (double)istruth;
		VarName[variables[i]]["CutStepTruth"] = (double)cutstep_truth;
        // create binning for this variables
        vector<double> varbinning;
        for(int index =0; index<nbin+1; index++)
            varbinning.push_back(xmin + index*(xmax-xmin)/nbin*1.0);
        VarBinning[variables[i]] = varbinning;
    }
}

//<AnalysisVar::InitVar>
void AnalysisVar::InitVar(string varlist, int nbin, vector<double> &binning, int cutstep, int option, int vec, bool istruth, int cutstep_truth) {
    vector<string> variables;
    InitStrVec(variables, varlist, ",");
    for(int i=0; i<(int)variables.size(); i++) {
		// Note that "CutStep" specify at which cut stage the histogram will be filled
		// For truth variables with IsTruth==true, the "CutStepTruth" will specify where to fill in truth selection
		// Please note by default the truth variables will be automatically filled at reco runnings at the step indicated by "CutStep"
        VarName[variables[i]]["NBins"]=nbin;
        VarName[variables[i]]["Xmin"]=binning[0];
        VarName[variables[i]]["Xmax"]=binning[binning.size()-1];
        VarName[variables[i]]["Option"]=(double)option;        
        VarName[variables[i]]["CutStep"]=(double)cutstep;   
        VarName[variables[i]]["Vector"]=(double)vec;
		VarName[variables[i]]["IsTruth"] = (double)istruth;
		VarName[variables[i]]["CutStepTruth"] = (double)cutstep_truth;
        // create binning for this variables
        VarBinning[variables[i]] = binning;
    }
}


//<AnalysisVar::InitSetting>
void AnalysisVar::InitSetting(MapType2_Int& setmap, string setname, string settings) {
    vector<string> vsettings;
    InitStrVec(vsettings, settings, ",");
    for(int i=0; i<(int)vsettings.size(); i++) {
        vector<string> pairs;
        InitStrVec(pairs,vsettings[i],"=");
        if(pairs.size()<2) {
            cout<<"Error in setting: can not parse "<<vsettings[i]<<endl; 
            exit(-1);
        }
        //if the setting key exist, should remove the old
        if(setmap[setname].find(pairs[0]) != setmap[setname].end()) {
            setmap[setname].erase(pairs[0]);
        }
        setmap[setname][pairs[0]]=atoi(pairs[1].c_str());
    }
}

//<AnalysisVar::InitSetting>
void AnalysisVar::InitSetting(MapType2_Double& setmap, string setname, string settings) {
    vector<string> vsettings;
    InitStrVec(vsettings, settings, ",");
    for(int i=0; i<(int)vsettings.size(); i++) {
        vector<string> pairs;
        InitStrVec(pairs,vsettings[i],"=");
        if(pairs.size()<2) {
            cout<<"Error in setting: can not parse "<<vsettings[i]<<endl; 
            exit(-1);
        }
        setmap[setname][pairs[0]]=atof(pairs[1].c_str());
    }
}

//<AnalysisVar::Init2DHelper>
void AnalysisVar::Init2DHelper(string var1, int nbinx, double xmin, double xmax, string var2, int nbiny, double ymin, double ymax, int cutstep) {
        
    string histo_name=var1+"_"+var2;
    vector<double> setting_var1, setting_var2;
    setting_var1.push_back(1);
    setting_var1.push_back(nbinx);
    //setting_var1.push_back(xmin);
    //setting_var1.push_back(xmax);
    for(int index=0; index<nbinx+1; index++)
        setting_var1.push_back(xmin + (xmax-xmin)/nbinx*1.0);
    setting_var1.push_back(cutstep);
    setting_var2.push_back(2);
    setting_var2.push_back(nbiny);
    // setting_var2.push_back(ymin);
    //setting_var2.push_back(ymax);    
    for(int index=0; index<nbiny+1; index++)
        setting_var2.push_back(ymin + (ymax-ymin)/nbiny*1.0);    
    setting_var2.push_back(cutstep);    
    helper_2D[histo_name][var1]=setting_var1;
    helper_2D[histo_name][var2]=setting_var2;
}

//<AnalysisVar::Init2DHelper>
void AnalysisVar::Init2DHelper(string var1, int nbinx, vector<double> &binning1, string var2, int nbiny, vector<double> &binning2, int cutstep) {
        
    string histo_name=var1+"_"+var2;
    vector<double> setting_var1, setting_var2;
    setting_var1.push_back(1);
    setting_var1.push_back(nbinx);
    //setting_var1.push_back(xmin);
    //setting_var1.push_back(xmax);
    for(int index=0; index<(int)binning1.size(); index++)
        setting_var1.push_back(binning1[index]);
    setting_var1.push_back(cutstep);
    setting_var2.push_back(2);
    setting_var2.push_back(nbiny);
    // setting_var2.push_back(ymin);
    //setting_var2.push_back(ymax);    
    for(int index=0; index<(int)binning2.size(); index++)
        setting_var2.push_back(binning2[index]);
    setting_var2.push_back(cutstep);    
    helper_2D[histo_name][var1]=setting_var1;
    helper_2D[histo_name][var2]=setting_var2;
}

//<AnalysisVar::Init3DHelper>
void AnalysisVar::Init3DHelper(string var1, int nbinx, double xmin, double xmax, string var2, int nbiny, double ymin, double ymax, string var3, int nbinz, double zmin, double zmax, int cutstep) {
        
    string histo_name=var1+"_"+var2+"_"+var3;
    vector<double> setting_var1, setting_var2, setting_var3;
    setting_var1.push_back(1);
    setting_var1.push_back(nbinx);
    //setting_var1.push_back(xmin);
    //setting_var1.push_back(xmax);
    for(int index=0; index<nbinx+1; index++)
        setting_var1.push_back(xmin + (xmax-xmin)/nbinx*1.0);
    setting_var1.push_back(cutstep);
    setting_var2.push_back(2);
    setting_var2.push_back(nbiny);
    // setting_var2.push_back(ymin);
    //setting_var2.push_back(ymax);    
    for(int index=0; index<nbiny+1; index++)
        setting_var2.push_back(ymin + (ymax-ymin)/nbiny*1.0);    
    setting_var2.push_back(cutstep);    
    setting_var3.push_back(3);
    setting_var3.push_back(nbinz);
    // setting_var3.push_back(zmin);
    //setting_var3.push_back(zmax);    
    for(int index=0; index<nbinz+1; index++)
        setting_var3.push_back(zmin + (zmax-zmin)/nbinz*1.0);    
    setting_var3.push_back(cutstep);     
    helper_3D[histo_name][var1]=setting_var1;
    helper_3D[histo_name][var2]=setting_var2;
    helper_3D[histo_name][var3]=setting_var3;
}


//<AnalysisVar::Init3DHelper>
void AnalysisVar::Init3DHelper(string var1, int nbinx, vector<double> &binning1, string var2, int nbiny, vector<double> &binning2, string var3, int nbinz, vector<double> &binning3, int cutstep) {
        
    string histo_name=var1+"_"+var2+"_"+var3;
    vector<double> setting_var1, setting_var2, setting_var3;
    setting_var1.push_back(1);
    setting_var1.push_back(nbinx);
    //setting_var1.push_back(xmin);
    //setting_var1.push_back(xmax);
    for(int index=0; index<(int)binning1.size(); index++)
        setting_var1.push_back(binning1[index]);
    setting_var1.push_back(cutstep);
    setting_var2.push_back(2);
    setting_var2.push_back(nbiny);
    // setting_var2.push_back(ymin);
    //setting_var2.push_back(ymax);    
    for(int index=0; index<(int)binning2.size(); index++)
        setting_var2.push_back(binning2[index]);
    setting_var2.push_back(cutstep);    
    setting_var3.push_back(3);
    setting_var3.push_back(nbinz);
    // setting_var3.push_back(zmin);
    //setting_var3.push_back(zmax);    
    for(int index=0; index<(int)binning3.size(); index++)
        setting_var3.push_back(binning3[index]);  
    setting_var3.push_back(cutstep);     
    helper_3D[histo_name][var1]=setting_var1;
    helper_3D[histo_name][var2]=setting_var2;
    helper_3D[histo_name][var3]=setting_var3;
}




//<AnalysisVar::SetVar>
void AnalysisVar::SetVar(string name, double value) {
    Var[name]=value;
}

/*
double AnalysisVar :: scaleAndSmear( double oldPt, double eleEta, TH1F *m_alpha_histo, TH1F *m_dE_histo ) {

  //double eleEta = ele->eta();
  //double oldPt  = ele->p4().Pt();

  int binN = m_alpha_histo->FindBin(eleEta);
  float alpha = m_alpha_histo->GetBinContent(binN);
  float quadrE = m_dE_histo->GetBinContent(binN);

  float newPt = oldPt/alpha;
  float deltaE = 0;
  //if ( m_doSmear )
  deltaE = rndnr.Gaus(0,TMath::Sqrt(quadrE)*newPt);

  newPt = newPt+deltaE;

  return newPt;

}
*/
