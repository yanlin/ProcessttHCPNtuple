//<run_MyAnalysis.C>
//<The run script for analysis codes>
//<Usage: root -l -b -q run_MyAnalysis.C() or Compile it first and then run the executable>

#include <time.h>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdlib.h>
#include <TTree.h>
#include <TChain.h>
#include "MyAnalysis.h"
#include "TreeBranch.h"
using namespace std;

//int run_MyAnalysis(string stream = "MC", string treename = "tree_NOMINAL", string inputlist = "input_list.txt", string treenamewt = "truth") {
int run_MyAnalysis(string inputlist = "input_list.txt", string stream = "MC", string treename = "output", string treenamewt = "truth") {

    //<A little bit explanation here>
    //stream name:
    //"MC" or "Muons"/"Egamma"
    //tree name: 
    //change the default if neccessary
    //inputlist:
    //put your file list into "input_list.txt"
    
    //<Evaluate time cost>
    double timedif = 0.;
    time_t start,end;
    time(&start);  
    struct tm * timeinfo;
    timeinfo = localtime(&start);
    string current_time;
    current_time = asctime(timeinfo);
  
    //<Display job information, only for linux system>
    string user, host;
    if(getenv("USER") != NULL) user=getenv("USER");
    if(getenv("HOSTNAME") != NULL) host=getenv("HOSTNAME");
    cout<<endl;
    cout<<"<----------------------->"<<endl;
    cout<<"<Start to run MyAnalysis>"<<endl;
    cout<<"<----------------------->"<<endl;
    cout<<endl;
    cout<<"User: "<<user<<endl;
    cout<<"Host: "<<host<<endl;
    cout<<"Time: "<<current_time<<endl;
    cout<<endl;
    
    //<Define root TChain, tree name comes from input>
    TChain *chain = new TChain(treename.c_str());    
    
    //<Get input list>
    ifstream filelist;
    filelist.open(inputlist.c_str());
    if(!filelist.good()) {
        cout<<"ERROR: Cannot find the input filelist, now quit!"<<endl;
        exit(-1);
    }    
    
    //<Add trees into TChain>
    string file;
    while(!filelist.eof()) {
        getline(filelist,file);        
        if(file.size()==0) continue; //remove the blank lines
        cout<<"Add file \""<<file<<"\""<<endl;
        //if dcache, use dcap://dcap.aglt2.org as prefix
        if(file.find("/pnfs")!=string::npos)
            file =  "dcache:" + file;
        //Check if the sample is used for training
        if(file.find("ttH_a0rw")!=string::npos || file.find("tHjb_a0rw")!=string::npos || file.find("tWH_a0rw")!=string::npos || file.find("ttH_a90rw")!=string::npos || file.find("tHjb_a90rw")!=string::npos || file.find("tWH_a90rw")!=string::npos) stream = "TrainingSample";
        if(file.find("data")!=string::npos) stream = "Data";

        chain->Add(file.c_str());
    }   
     
    //<Set up loading options to make it loop faster>
    chain->SetCacheSize(10000000);
    TreeBranch(chain);

    //cout<<"get branch: "<<TreeBranch.GetBranch("muonType")->size()<<endl; 
    //<Process events, do analysis>
    //<Refer to the analysis class for details>
    MyAnalysis *ww = new MyAnalysis();
    ww->Init(chain);
    ww->Begin(chain, stream);
    cout<<"After Begin"<<endl;
    long nevts = chain->GetEntries();

    for(long i=0; i<nevts; i++) {

        // for testing purpose
	//if (i >= 500) break;
        //cout<<"Process "<<i<<" Events"<<endl;        

      
        //load the D3PD contents
        ww->LoadEntry(i);
		
	//Process entry
	ww->ProcessEntry(i);

	for (int index = 0; index < (int)ww->SETNAME.size(); index++) {
                string setname = ww->SETNAME[index];
		ww->Selection(setname);
		ww->FinishSelection(setname);
	}

    }
    ww->Terminate();
 
    //<Job end, calculate how much time elapsed>
    time(&end);
    struct tm * timeinfo2;
    timeinfo2 = localtime(&end);
    string end_time;
    end_time = asctime (timeinfo);
    timedif = difftime(end,start);
    cout<<endl;
    cout<<endl;
    cout<<"Job End Time: "<<end_time<<endl;
    if(timedif>3600) {cout<<"Time Cost: "<<setprecision(3)<<timedif/3600.<<" hours"<<endl;}
    else if(timedif>60) {cout<<"Time Cost: "<<setprecision(3)<<timedif/60.<<" minutes"<<endl;}
    else {cout<<"Time Cost: "<<setprecision(3)<<timedif<<" seconds"<<endl;}
    
    //<delete the instance at the end>
    delete ww;
    delete chain;

    //<The END>
    return 0;
}

int main(int argc, char *argv[]) {

    if(argc==1) run_MyAnalysis();
    else if(argc==2) run_MyAnalysis(argv[1]);
    else if(argc==3) run_MyAnalysis(argv[1],argv[2]);    
    else if(argc==4) run_MyAnalysis(argv[1],argv[2],argv[3]);    
    else cout<<"Error => Can not have so many arguments!!! "<<argc-1<<endl;
    return 0;
}
