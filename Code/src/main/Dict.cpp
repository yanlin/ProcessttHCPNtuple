// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME maindIDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "NtupleBase.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_NtupleBase(void *p = 0);
   static void *newArray_NtupleBase(Long_t size, void *p);
   static void delete_NtupleBase(void *p);
   static void deleteArray_NtupleBase(void *p);
   static void destruct_NtupleBase(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::NtupleBase*)
   {
      ::NtupleBase *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::NtupleBase >(0);
      static ::ROOT::TGenericClassInfo 
         instance("NtupleBase", ::NtupleBase::Class_Version(), "NtupleBase.h", 22,
                  typeid(::NtupleBase), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::NtupleBase::Dictionary, isa_proxy, 4,
                  sizeof(::NtupleBase) );
      instance.SetNew(&new_NtupleBase);
      instance.SetNewArray(&newArray_NtupleBase);
      instance.SetDelete(&delete_NtupleBase);
      instance.SetDeleteArray(&deleteArray_NtupleBase);
      instance.SetDestructor(&destruct_NtupleBase);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::NtupleBase*)
   {
      return GenerateInitInstanceLocal((::NtupleBase*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::NtupleBase*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr NtupleBase::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *NtupleBase::Class_Name()
{
   return "NtupleBase";
}

//______________________________________________________________________________
const char *NtupleBase::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::NtupleBase*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int NtupleBase::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::NtupleBase*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *NtupleBase::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::NtupleBase*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *NtupleBase::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::NtupleBase*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void NtupleBase::Streamer(TBuffer &R__b)
{
   // Stream an object of class NtupleBase.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(NtupleBase::Class(),this);
   } else {
      R__b.WriteClassBuffer(NtupleBase::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_NtupleBase(void *p) {
      return  p ? new(p) ::NtupleBase : new ::NtupleBase;
   }
   static void *newArray_NtupleBase(Long_t nElements, void *p) {
      return p ? new(p) ::NtupleBase[nElements] : new ::NtupleBase[nElements];
   }
   // Wrapper around operator delete
   static void delete_NtupleBase(void *p) {
      delete ((::NtupleBase*)p);
   }
   static void deleteArray_NtupleBase(void *p) {
      delete [] ((::NtupleBase*)p);
   }
   static void destruct_NtupleBase(void *p) {
      typedef ::NtupleBase current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::NtupleBase

namespace {
  void TriggerDictionaryInitialization_Dict_Impl() {
    static const char* headers[] = {
"NtupleBase.h",
0
    };
    static const char* includePaths[] = {
"main",
"",
"",
"/cvmfs/sft.cern.ch/lcg/releases/ROOT/6.14.04-0d8dc/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/y/yanlin/HyyAnalysis/2019.11.04_ProcessttHCPNtuple/Code/src/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "Dict dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$NtupleBase.h")))  NtupleBase;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "Dict dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "NtupleBase.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"NtupleBase", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("Dict",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_Dict_Impl, {}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_Dict_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_Dict() {
  TriggerDictionaryInitialization_Dict_Impl();
}
