//<MyAnalysis.h>
//<The header file for analysis>

#ifndef MyAnalysis_h
#define MyAnalysis_h

//<Common Header>
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TSelector.h>
#include <TLorentzVector.h>
#include <TRandom3.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <iomanip>
#include <string>
#include "NtupleBase.h"
#include "AnalysisVar.h"
#include "TMVA/Reader.h"
#include "TRandom.h"

//<Predefined Constants>
const double PI=3.1415926;
const double ZMass=91.1876e3; //MeV
const double m_mass=105.658367; //MeV
const double e_mass = 0.510998910; //MeV
const double Unit_GeV = 1000.; //MeV


//<Analysis Class>
class MyAnalysis : public NtupleBase, public AnalysisVar {
public :

    //<Control Parameters and Variables>
    //<See details in AnalysisVar Class>
    int NEVTS; //count total number of processed events    
    bool VERBOSE; //verbose or not
    string STREAM; //indicating event from which stream
    
    mutable TRandom3 rndnr;
    //<Inherited from Base class>
    //<Seprate entry loading and event processing function>
    //<So that one can load once but run multiple configurations>
    MyAnalysis(TTree * /*tree*/ =0) {}
    virtual ~MyAnalysis() { }
    void    Begin(TTree *tree, string stream);
    bool    LoadEntry(long entry);
    bool    ProcessEntry(long entry);
    bool    Selection(string set);
	bool	    FinishSelection(string set);
    void    Terminate();
    double scaleAndSmear( double oldPt, double eleEta, TH1F *m_alpha_histo, TH1F *m_dE_histo );
};

#endif
