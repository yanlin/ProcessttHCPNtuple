//<A simple program to slim Root tree header>
//<Yusheng Wu, 2011>
//<wyusheng@umich.edu>

#ifndef TreeBranch_h
#define TreeBranch_h

#include <iostream>
#include <fstream>
#include <string>
#include <TTree.h>
#include <vector>
using namespace std;

int TreeBranch(TTree *tree, int DataOrMC=0, string config="main/TreeBranch.txt");

#endif
