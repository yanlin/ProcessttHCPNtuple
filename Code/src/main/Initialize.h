//<Initialize.h>
//<Initialization for analysis>
#include "MyAnalysis.h"

#include <iostream>
#include <fstream>

//<-------------------------->
//<Analysis Codes, Initialize>
//<-------------------------->


//<Analysis::Begin>
//<Initialize variables, histograms, trees before acutal event loop>
void MyAnalysis::Begin(TTree *tree, string stream) {
    
    //<------------------->
    //<Overall setting map>
    //<------------------->

    //<common variables>
    STREAM = stream;
    NEVTS=0;
    VERBOSE=false;

    // prepare the strings for systematic variation
    vector<string> setnames;
    vector<string> setvalues;

    SETNAME.push_back("NOM");
    InitSetting(SETTING, "NOM", "dotree=0,fillstep=0,doclone=0,dohistogram=1");

    for(int i=0; i<(int)setnames.size(); i++) {
        //for each variation, initialize the default setup first
        //Part1
        string theset=setnames[i];
        SETNAME.push_back(theset);
        InitSetting(SETTING, theset, "dotree=0,fillstep=0,doclone=0,dohistogram=0");
        //then modify
        string thechange = setvalues[i];
        InitSetting(SETTING, theset, thechange);
    }
	
    //<Set up cuts, variables, histograms and trees>
    InitStrVec(CHN,"incl,cate0,cate1,cate2,cate3,cate4,cate5,cate6,cate7,cate8,cate9,cate10,cate11,cate12,cate13,cate14,cate15");
    InitSTEP(STEP_cut,"Selected");
    CreateCountingMap();
 
    Double_t pi = acos(-1.0);
    //Event Common variables
    InitVar("m_yy",550,105,160,0);
    InitVar("pt_H,ph_pt1,ph_pt2",1000,0,500,0);
    InitVar("eta_H",100,-5,5,0);
    InitVar("BDT_score",100,0,1,0);
     
    //<Trees and Root Files>   
    CreateAllFiles("ttHAna"); //will generate Analysis + _tree.root / _histo.root
        
    CreateTreeVarHistoMap();    

}

