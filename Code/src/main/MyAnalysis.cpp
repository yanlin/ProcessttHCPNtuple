//<MyAnalysis.C>
//<Analysis code>

#include "Initialize.h"
#include <iostream>
#include <fstream>


//<--------------------->
//<Analysis Codes, Start>
//<--------------------->


//<Analysis::LoadEntry>
bool MyAnalysis::LoadEntry(long entry) {
    //fChain->GetEntry(entry);
    fReader.GetCurrentEntry();
}


//<Analysis::ProcessEntry>
bool MyAnalysis::ProcessEntry(long entry) {
    
    fReader.SetLocalEntry(entry);
    //<Count Events>
    NEVTS++;
    //if(run==0) return kTRUE;
    //cout<<"Processed "<<entry<<"events"<<endl;
    if(entry%500 == 0) cout<<"Processed "<<entry<<"events"<<endl; 
    //cout<<"Processed "<<entry<<"events"<<endl;  
    
  
    //if(VERBOSE)
        //cout<<"Event Index: "<<entry<<", RunNumber: "<<run<<", EventNumber: "<<event;
    
    //<Clear StoreV variables>
    ClearVariables(VVarStore);
    ClearVariables(VarStore);

    return true;
}


//<Analysis::TruthSelection>
//Perform selection at truth level
bool MyAnalysis::Selection(string set) {

	//<Initialize variables and selection flags for each event>
	//<Clear good object vector as well>
	ClearVariables(Var);
	ClearVariables(VVar);
	ClearFlags(FLAG_cut);
	ClearFlags(FLAG_cut_temp);
	ClearWeight(Evt_Weight);

        bool blind = true;

        if (*isPassed <= 0) return true; 
        if (STREAM == "TrainingSample" && *fold != 0) return true;
        if (blind && STREAM == "Data" && *m_mgg > 120. && *m_mgg < 130. ) return true;
 
        //Unselected events
        SetFlag(FLAG_cut_temp, "Selected", "cate0", *CP_category == 0);
        //Nine had. categories
        SetFlag(FLAG_cut_temp, "Selected", "cate1", *CP_category == 1);
        SetFlag(FLAG_cut_temp, "Selected", "cate2", *CP_category == 2);
        SetFlag(FLAG_cut_temp, "Selected", "cate3", *CP_category == 3);
        SetFlag(FLAG_cut_temp, "Selected", "cate4", *CP_category == 4);
        SetFlag(FLAG_cut_temp, "Selected", "cate5", *CP_category == 5);
        SetFlag(FLAG_cut_temp, "Selected", "cate6", *CP_category == 6);
        SetFlag(FLAG_cut_temp, "Selected", "cate7", *CP_category == 7);
        SetFlag(FLAG_cut_temp, "Selected", "cate8", *CP_category == 8);
        SetFlag(FLAG_cut_temp, "Selected", "cate9", *CP_category == 9);
        //Six lep. categories
        SetFlag(FLAG_cut_temp, "Selected", "cate10", *CP_category == 10);
        SetFlag(FLAG_cut_temp, "Selected", "cate11", *CP_category == 11);
        SetFlag(FLAG_cut_temp, "Selected", "cate12", *CP_category == 12);
        SetFlag(FLAG_cut_temp, "Selected", "cate13", *CP_category == 13);
        SetFlag(FLAG_cut_temp, "Selected", "cate14", *CP_category == 14);
        SetFlag(FLAG_cut_temp, "Selected", "cate15", *CP_category == 15);
         
        Var["m_yy"] = *m_mgg;
        Var["pt_H"] = *pt_H;
        Var["ph_pt1"] = *ph_pt1;
        Var["ph_pt2"] = *ph_pt2;
        Var["eta_H"] = *eta_H;
        //Var["BDT_score"] = *(BDTG_cp_10\.8\.0);

	return true;
}



//<Analysis::FinishSelection>
//<Close action for each set of running jobs>
bool MyAnalysis::FinishSelection(string set) {
    
    //Do counting and Get cut steps, flags and settings
    vector<string> cutstep;
    MapType2_Int setting, flag;
    DoCountingCut(set);
    cutstep = STEP_cut;
    setting = SETTING;
    flag = FLAG_cut;
    
    //cout<<"nor_factor: "<<nor_factor<<endl;	
    // Set up weights for filling histograms
    for(int i=0; i<(int)STEP_cut.size(); i++) {
        string cut = STEP_cut[i];
        Evt_Weight[cut][0] = *weight_for_analysis*138.97;
        if (STREAM == "TrainingSample") Evt_Weight[cut][0] = Evt_Weight[cut][0]*4;
    }
        //cout<<"cut:"<<cut<<" code weight: "<<Evt_Weight[cut][0]<<endl;

    if (VERBOSE) {
	string final_cut;
	for (int i = 0; i<(int)cutstep.size(); i++) {
		string cut = cutstep[i];
		if (flag["incl"][cut] == 1) final_cut = cut;
	}
	cout << ", Pass Cut: " << final_cut << endl;
    }

        //cout<<"test 3"<<endl;

    //<Fill Histograms>
    if (setting[set]["dohistogram"] == 1)
	FillHistograms(set);

        //cout<<"test 4"<<endl;

	//<Fill Trees>
	if (setting[set]["dotree"] == 1) {
		//if need to fill tree, usually should assign a number to identify at which cut step to fill
		int istep = setting[set]["fillstep"];
		string step = cutstep[istep];
		//fill
		if (flag["incl"][step])
			Tree[set]->Fill();
	}
}


//<Analysis::SlaveTerminate>
void MyAnalysis::Terminate() {

    //<Write ROOT Files>
    HistoFile->cd();
    HistoFile->Write();
    HistoFile->Close();
            
    //<Cut Flow Output>
    for(int set=0; set<(int)SETNAME.size(); set++) {
        for(int i=0; i<(int)CHN.size(); i++) {
            string chn = CHN[i];
            string filename = "log_eff_" + chn + "_" + SETNAME[set] + ".txt";
            ofstream file(filename.c_str());
            if(file.is_open()) {
                for(int j=0; j<(int)STEP_cut.size(); j++) {
                    string cut = STEP_cut[j];
                    file<<cut<<"="<<CNT_cut[SETNAME[set]][chn][cut].num<<
                        "+/-"<<CNT_cut[SETNAME[set]][chn][cut].err<<endl;
                }
                file.close();
            }
            else {
                cout<<"Can not open file "<<filename<<endl;
                exit(-1);
            }    
        }
    
    }//for:set


    cout<<"Total Process Number of Events: "<<NEVTS<<endl;
    
}

//<------------------->
//<Analysis Codes, END>
//<------------------->

