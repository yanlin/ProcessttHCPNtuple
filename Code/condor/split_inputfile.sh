#!/bin/bash

# A simple script to split input_list

# Firstly, input_list.txt should exist
if [ ! -f "input_list.txt" ]; then
  echo "input_list.txt does not exist, please check"
  exit 1
fi

# Moniter if user specified number of lines for each part
if [ $# == 1 ] && [ $1 -gt 1 ] && [ $1 -lt 1000 ]; then
  line_per_part=$1
else 
  line_per_part=5
fi

# Auto decide how much to split
# Default is 5 file per job
# maximum size for one job is 5GB
nfile=0
nsize=0

while read line; do
 size=`du -s $line`
 fsize=`echo $size | cut -f1 -d' '`
 nfile=`expr $nfile + 1`
 nsize=`expr $nsize + $fsize`
done < input_list.txt

avg_size=`echo "$nsize / $nfile" | bc`
if [ $avg_size -lt 500 ]; then line_per_part=1000;
elif [ $avg_size -lt 1000 ]; then line_per_part=500;
elif [ $avg_size -lt 2000 ]; then line_per_part=250;
elif [ $avg_size -lt 5000 ]; then line_per_part=100;
elif [ $avg_size -lt 10000 ]; then line_per_part=50;
elif [ $avg_size -lt 20000 ]; then line_per_part=20;
elif [ $avg_size -lt 50000 ]; then line_per_part=10;
elif [ $avg_size -lt 200000 ]; then line_per_part=10;
elif [ $avg_size -lt 500000 ]; then line_per_part=7;
elif [ $avg_size -lt 1000000 ]; then line_per_part=4;
elif [ $avg_size -lt 2000000 ]; then line_per_part=2;
else line_per_part=1; fi

line_per_part=1

# Get N of files in input_list.txt
nline=`wc -l input_list.txt | cut -d' ' -f 1`

nparts=0.0
nparts=`echo "scale=6; $nline*1.0/$line_per_part" | bc`
nparts_int=`echo $nparts | cut -d'.' -f 1`
nparts_decimal=`echo $nparts | cut -d'.' -f 2`

if [ $nparts_decimal -gt 0 ]; then
  nparts_int=`expr $nparts_int + 1`
fi

echo "INFO ==> input_list (total $nline lines) will be split into $nparts_int parts"

# Split input_list.txt
split -d -l $line_per_part -a 4 input_list.txt input.splitted_list
