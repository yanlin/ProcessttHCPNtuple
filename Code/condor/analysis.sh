#!/bin/bash

# Receive Arguments from command lines
path=$1
workpath=$2 
user=$3
STREAM=$4
PATH_SRC=$5
jobNode=`hostname`

# Create an uniquename for the run folder
unique_tag1=`basename $path | cut -d'.' -f1-2`
unique_tag2=`date +%s`
localdir="/tmp/${user}/mytest/${workpath}-${unique_tag1}-${unique_tag2}"
workdir=$path"/"$workpath

echo "..................................................................................."
echo "  Runing Jobs using host $jobNode in directory $workdir..."
echo "..................................................................................."

#### create local working directory and copy input files to local disk

mkdir -vp $localdir

cp -rf $PATH_SRC                           $localdir/
cp -rf $workdir/input_list.txt               $localdir/src

#### change to local directory and run jobs
cd $localdir/src

#### setting root and libraries
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"

### run job
./run.exe input_list.txt

#### copy output files back to current working directory
cp log_eff*.txt                     $workdir
cp *_histo.root                     $workdir


#### delete information in local disk
cd /tmp/$user/mytest
rm -r -f $localdir

########################################################

ExitStatus=$?
if [ $ExitStatus -ne 0 ]; then
  echo "e-id job failed with status $ExitStatus ..."
  exit $ExitStatus
fi

exit 0

